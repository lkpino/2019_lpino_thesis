
\prelimpages
 
%
% ----- copyright and title pages
%
\Title{Methods for harmonizing and calibrating quantitative mass spectrometry experiments}
\Author{Lindsay Kristina Pino}
\Year{2019}
\Program{Genome Sciences}

\Chair{Michael J. MacCoss}{Professor}{Genome Sciences}
\Chair{William Stafford Noble}{Professor}{Genome Sciences}
\Signature{Andrew N. Hoofnagle}

\copyrightpage

\titlepage  
%
% ----- abstract
%
\setcounter{page}{-1}
\abstract{%
	
The field of mass spectrometry proteomics has made great technological progress, and these techniques are now being used to address essential questions in basic biology and are increasingly being used on samples of clinical significance. In particular, the development of data independent acquisition mass spectrometry (DIA-MS) has made it possible to measure tens of thousands of peptides from a protein digest in a 1-2 hour time scale. As generating larger and larger proteomic data sets becomes easier and easier, questions about normalizing batch effects and assessing data quality have arisen in the mass spectrometry community. In the following chapters, I describe three projects that aimed to address various challenges associated with scaling up quantitative mass spectrometry experiments. 

I first introduce the need for reference materials for mass spectrometry proteomics. In Chapter 2, I describe a single-point external calibration strategy to calibrate signal intensity measurements to a common reference material, which places MS measurements on the same scale and harmonizes signal intensities between instruments, acquisition methods, and sites. In Chapter 3, I extend the reference material calibration approach to multi-point calibration and demonstrate the consequences of linearity in quantitative analyses. We apply this approach to yeast lysate, human cerebrospinal fluid, and formalin-fixed paraffin-embedded samples. In Chapter 4, I apply the methods developed in the previous two chapters to investigate the yeast proteome response to genetic and environmental modulators of replicative lifespan. I show that the protein-level signatures associated with replicative lifespan extension suggest a higher-level response beyond protein abundances. Lastly, I present closing remarks and future directions in Chapter 5.


}
%
% ----- contents & etc.
%
\tableofcontents
\listoffigures
\listoftables  
%
% ----- acknowledgments
%
\acknowledgments{
This work would not have been possible without the support of many people. First, thank you to my advisors, Mike MacCoss and Bill Noble, for your guidance and encouragement not only on these thesis projects but my overall career development. To Mike, thank you for continuously pushing me past my self-imposed limitations and being the nucleus for many of my closest professional relationships; to Bill, thank you for believing in my potential and thank you for your patience while I came to realize it. Thank you to Andy Hoofnagle for your specific guidance on and scientific contributions to two of these thesis chapters. Also, I would like to thank the entirety of my thesis committee, Maitreya Dunham, Matt Kaeberlein, and Shao-En Ong, for your many valuable contributions to not only this work but my future directions. I hope to continue learning from you all.

The experimental and computational work in this thesis reflects my fortunate circumstance of being jointly trained in two labs. I'd like to thank Alex Hu, Andy Lin, Will Fondrie, and Wout Bittremieux of the Noble Lab for their assistance with programming and computational proteomics. In the MacCoss lab, I'd especially like to thank Han-Yin Yang for being a constant source of advice and camaraderie; and I'd also like to thank Brian Searle for becoming like a third thesis adviser to me both in scientific contributions and in professional development. The friendships I've made during my graduate career have also supported this work, and so I'm thankful to Damien Wilburn for many science talks over beers and especially to Hannah Pliner for the miles of hiking, backpacking, and dog park-walking.

I would not have considered an advanced degree without the encouragement of my past supervisors, Sue Abbatiello and Steve Carr.

Finally, thank you to my family for building me into the person I am today. Thank you to my siblings, Andrew and Katie, for many years of teaching me the importance of a good role model, and for being such good role models yourselves. Thank you to my parents, John and Elaine, for prioritizing our education even when it came at personal expense, and for your patience and unconditional love while I figured out my place in the world. Last, thank you to Alex for supporting my decision to go back to school by dropping everything, moving thousands of miles away to be here for me physically and emotionally through timepoint experiments, instrument failures, and procrastinated deadlines.

}

%
% ----- dedication
%
\dedication{
\begin{center}
	For all those family, friends, and colleagues who believed in me.
\end{center}
}

% end of the preliminary pages
