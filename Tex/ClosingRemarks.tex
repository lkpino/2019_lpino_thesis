% ========== Closing Remarks
\graphicspath{ {./FutureDirections/images/} } 

\chapter {Closing Remarks}
Quantitative mass spectrometry experiments have historically been treated independently as individual assays developed and optimized for a specific laboratory and for a specific purpose \citep{carr_targeted_2014}. However, the growing adaption of DIA/SWATH methods now enable studies to build upon previous work and opens up the possibility of larger scale experiments across laboratories and time. With increasing experimental scales comes discussions about how best to integrate and combine these data sets.  

\section{Scaling quantitative mass spectrometry data responsibly}
One prospect that I find exciting about these large-scale endeavors is the re-utilization of data. Even within modest experimental efforts, the practical limitations of sample preparation -- such as homogenization methods or the number of slots available in a centrifuge -- preclude the complete randomization of more than just a couple dozen samples at a time, forcing experimentalists to split their samples into blocks that later reappear as batch effects during data analysis. 

As mass spectrometry proteomics groups embark on more ambitious experiments like the ProCan and NCI60 projects \citep{guo_rapid_2019}, I anticipate it will become more and more crucial to include external reference materials during experimental design. Even though these projects have already begun, incorporating a working reference and global reference may harmonize these data sets.

Reutilization of data would also benefit from building data repositories that cater specifically to quantitative proteomics. While there are many raw data repositories, these resources typically focus on shotgun DDA reanalyses, so while they provide the means to search for peptides in the repository, they don't typically support the targeted quantitative proteomics workflows. Building a repository specifically designed to assist the reuse and reanalysis of quantitative proteomics data would require a standardized quantitative data format, experimental meta data descriptors, and quality control/assurance approaches, but would enable researchers to build on prior work, rather than needlessly repeating past experiments.

\section{Bridging proteomics data generation and data analysis}
Before I began my graduate training, I recognized the rapid growth in quantitative proteomics data generation. I also realized that my classical training in biochemistry didn't include the necessary coding and statistical skills required to appropriately handle data at the scales it was being acquired, and so I resolved to gain these skills for myself in my predoctoral training. In doing so, I've positioned myself uniquely as a sort of liaison between the data generators and the data analysts in the mass spectrometry community. 

I expect that, for future generations of mass spectrometrists, training will include more computational and statistical coursework. Until this new generation gains majority in the community, however, I expect that the most popular quantitative proteomics frameworks will be those that are not only well documented but also have easily accessible tutorials and workshops. Because most current mass spectrometrists aren't comfortable with programming or command line tools, I think moving the most common computational frameworks into user-friendly GUIs or Docker containers will be an easier bridge between generations. Until then, the ability to liaison between data generators and data analysts will be an important role for methods development and software adaptation.



\section {Future Directions}

\subsection{External reference materials in mass spectrometry proteomics}
Because not all analytes may be present in a given reference material under a single physiological condition, it will be important to build global reference materials that include all possible analytes. This may take the physical form of consensus pool mixtures made from a reference under multiple perturbations or disease states so that even condition-specific analytes are detectable. Then, working references with just a subset of all possible detected analytes would be used in individual experiments, but calibrated periodically to the physical global reference.

Alternatively, computational solutions may be able to create \textit{in silico} global references from imputing across multiple working references that represent a range of physiological conditions. This might take the form of something like current retention time alignment approaches, but instead of retention times, analyte abundances would be aligned across working references.

\subsection{Detection and quantification as independent processes}
As I demonstrated in Chapter 3, detection of a peptide doesn't imply that the peptide is quantitative. While the quantitative proteome appears to be a subset of the detected proteome, this may just be an artifact of the order of operations, which first relies on detection before quantitative assessment. In the future, it would be interesting to see this assumption challenged. That is, rather than detecting peptides first, it would be interesting to see a process that instead searches data for features that display quantitative properties, then perform detection on those selected features. Recent work has been done along these ideas for DDA analysis \citep{the_focus_2019}, but I think the scope of the idea could be expanded to DIA especially.


\subsection{Molecular phenotypes beyond peptide abundances}
Using quantitative proteomics even in presumably well-studied systems has revealed novel insights. For example, although genomics has already determined several gene-level subtypes of colon cancer, quantitative proteomics further clustered those subtypes based on their proteomic signatures \citep{zhang_proteogenomic_2014}. Beyond applications in cancer, I foresee quantitative proteomics being used to further understand phenomena that cannot be explained by the genotype alone such as cellular differentiation, cardiovascular disease, and neurodegenerative disease.

Although some disease states and phenotypes are driven by differential protein abundances, I think even more molecular phenotypes are described by higher-level molecular interactions. I don't think that merely measuring proteome-wide abundances will answer all our questions about the status or mechanism of complex phenotypes. Even if quantitative proteomics alone is unable to explain these phenomena, I think that these experiments will generate hypotheses for futher investigation. For example, although my work to model molecular phenotypes associated with yeast replicative life span in Chapter 4 was inconclusive on its own, the data itself is trustworthy and the conclusions do call into question previous work and new hypotheses to investigate.

This means that in the future quantitative proteomics may need to be paired with other technologies such as proximity-labeling approaches, post-translational modification detection, or even sequencing-based technologies like ATAC-seq which describe chromatin structure. Hypotheses generated by large scale experiments such as those used in this thesis may also need to be structurally or mechanistically validated using more traditional biochemical and genetic approaches.


\section{Conclusion}
Quantitative proteomics is poised to help bridge the gap between the genotypic information gleaned from DNA sequencing and the phenotypic presentations of biological structure and function. In this dissertation, I focused on data independent acquisition methods for quantitative proteomics due to the unprecedented scales of data that this approach enables, but many of the ideas and concepts explored here are generalizable to any analytical method. As larger and more ambitious quantitative proteomics experiments are undertaken, I anticipate that the analytical chemistry fundamentals demonstrated here will facilitate the robust and accurate molecular measurements required for biological advances.


