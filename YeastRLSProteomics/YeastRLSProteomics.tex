% ========== Yeast RLS Proteomics: Calorie Restriction and Aging K/O projects
\graphicspath{ {./YeastRLSProteomics/images/} } 

\chapter {Molecular phenotyping of the yeast \\replicative life span response to \\genetic and environmental modulators}

\section {Abstract}
Senescence and life span modulation are crucial biological processes for all cellular life, controlled through tightly regulated molecular mechanisms. Many of these mechanisms and their modulation are evolutionary conserved, including calorie restriction (also referred to as dietary restriction) and inhibition of the mTOR signaling pathway. While there are likely multiple cellular processes through which such life span modulators act, the full range of molecular phenotypes that cells display as they age under different contexts and genetic backgrounds remains to be characterized. In my dissertation research, I used state-of-the-art biochemical and proteomic tools to characterize the molecular phenotypes of life span extension in budding yeast (\textit{Saccharomyces cerevisiae}). Yeast are an excellent model system for this application, as replicative life span (RLS) – the number of times a mother cell buds before reaching senescence – is a quantifiable measure of aging that can be collected for yeast on experimental timescales. Further, the yeast genome is well annotated, there are a wealth of established resources for this model system such as the Saccharomyces Genome Database and the Saccharomyces Genome Deletion Collection, and finally, as a eukaryote, they are a much better model for human health compared to prokaryotic systems. 

Using our previously described mass spectrometry-based methods for quantifying the yeast proteome at scale, we were capable of creating quantitative proteomic signatures which we hoped to be useful as molecular phenotypes. Here, we applied these frameworks to construct molecular phenotypes of increased RLS in long-lived yeast mutants and to identify key proteins in life span extension through intervention methods.

\section{Introduction}
Aging in multicellular organisms is the accumulation of molecular, cellular, and finally tissue-specific alterations that ultimately culminates in the final phenotype \citep{tosato_aging_2007}. While aging itself is not considered a disease, certain disease states are associated with aging and better understanding the complexities behind these diseases requires an understanding of the multifaceted process of aging itself. Several distinct but not incompatible schools of thought about aging exist, including the free radical theory of aging \citep{harman_free_2003}, the \emph{inflamm-aging} theory of immunosenesence \citep{franceschi_inflamm-aging._2000}, and the mitochondrial damage theory \citep{cadenas_mitochondrial_2000}. As there is no single gene or pathway that culminates in the phenotype of aging or longevity, it is necessary to use experimentally-unbiased research techniques to examine aging as a global process. To this end, we propose to use quantitative proteomics to serve as objective measurands, as proteins are the primary functional biomolecules of the cell.

\paragraph{Significance to aging and aging-related disease.} 
Understanding complex aging-associated diseases is not possible without first understanding the molecular mechanisms that govern the basic biology of aging. As multicellular organisms age, distinct biochemical changes to their cells produce the phenotype referred to as aging. In humans, aging leads to increased risk of aging-associated disease such as neurodegeneration, cardiovascular disease, and cancer \citep{campisi_cellular_2003}. Several aging modulators, including specific genotypes and environmental perturbations, are evolutionarily conserved in single-celled eukaryotes and in multicellular organisms such as worms and mice \citep{smith_quantitative_2008}. This shared functionality suggests that the underlying molecular mechanisms are likely also conserved. We used advanced proteomic techniques to construct the underlying protein-level molecular phenotypes of long-lived mutants of budding yeast (\textit{Saccharomyces cerevisiae}), with a focus on defining how molecular phenotype changes in response to conserved genotype- and dose-dependent lifespan modulators.

\paragraph{Significance to systems and molecular biology.}
Modulators of lifespan such as calorie restriction (CR), and inhibition of mammalian target of rapamycin (mTOR) signaling have been found efficacious in mice \citep{mccay_effect_1935}, flies \citep{mair_demography_2003}, worms \citep{klass_aging_1977}, and yeast \citep{lin_calorie_2002}. The mechanism(s) by which these modulators extend lifespan remain poorly understood, but several pathways are generally evolutionarily conserved, including mitochondrial respiration, autophagy, and the signaling pathways of protein kinase A, mTOR, and AMP-activated protein kinase \citep{wasko_yeast_2013}. The presence of multiple functional categories in aging phenotypes is to be expected from such a complex biological process, and thus employing a global, unbiased approach to capture all involved pathways will provide valuable insights. By systematically evaluating these phenotypes of aging in the tractable genome of the yeast model system, we may be able to determine key proteins shared between these paths and identify molecular differences. 

These experiments are important both because they will elucidate the relationship between different aging modulators in yeast, and because they will improve confidence in quantitative protein signature profiling for future studies of yeast aging. In the subsequent chapter, we will use the molecular phenotypes discovered by this comparative analysis as a starting point for identifying genetic targets indicative of yeast longevity. We place special focus on genes and pathways that are conserved in multicellular organisms such as worms and mice.

\paragraph{Yeast replicative lifespan as a model for cellular aging.}
Yeast are an excellent model system for basic biology of aging studies because they enable genome-wide screens for molecular mechanisms associated with aging \citep{steinkraus_replicative_2008}. Yeast replicative lifespan (RLS) – the number of times a mother cell buds before reaching senescence – is a quantifiable, objective measure of aging that can be collected on experimental timescale \citep{mortimer_life_1959}. Further, the yeast genome is well annotated, there are a wealth of established resources for this model system such as the Saccharomyces Genome Database \citep{cherry_saccharomyces_2011} and the Saccharomyces Genome Deletion Collection \citep{giaever_functional_2002}, and finally, as a eukaryote, yeast is a more analogous model for human health than prokaryotic systems while retaining the experimental benefits of single-cellular models. Aging proteomics has also been successfully applied to yeast, revealing possible mechanisms of lifespan asymmetry in yeast mother / daughter cells \citep{yang_systematic_2015} and uncovering a novel mitochondrial unfolded protein response in prohibitin-deficient yeast \citep{schleit_molecular_2013}. We hypothesized our quantitative  proteomics approach would replicate these previously reported processes, reveal novel protein members of established processes, and discover processes not yet associated with yeast longevity. In this chapter, we construct molecular phenotypes of increased RLS in long-lived yeast perturbations using the quantitative proteomics methods described previously and identify key proteins in life span extension through these interventions.


\section{Methods}

\paragraph{Sample preparation} 

\subparagraph{Calorie restriction} Yeast strain BY4741 (MATa his3$\Delta$1 leu2$\Delta$0 met15$\Delta$0 ura3$\Delta$0) (Dharmacon) was grown overnight in YEPD media (~2\% glucose). The overnight culture was used to inoculate flask cultures of six media conditions of YEP with glucose added to each flask spanning the range of calorie restriction (specifically 2\% glucose, 1\%, 0.5\%, 0.05\%, 0.005\%, and 3\% glycerol), in biological triplicate. Glucose concentrations were measured by enzymatic D-glucose assay (r-biopharm, Germany). Cultures were grown to OD600 0.2 before harvest. Cell pellets were mechanically lysed with beadbeating and 8M urea. Lysates were reduced with DTT, alkylated with iodoacetamide, and digested with trypsin. 

\subparagraph{Gene deletions} Yeast single-gene deletion strains were chosen spanning a range of RLS modulation per McCormick 2015 \citep{mccormick_comprehensive_2015}. Special consideration was made to choose strains not documented as petite/slow-growing, and strains whose deleted gene was documented with the most interactions per the SGD \citep{cherry_saccharomyces_2011}. The final strains chosen were $\Delta${ubp8}, $\Delta${sgf73}, $\Delta${eos1}, $\Delta${idh2}, $\Delta${tor1}, $\Delta${adp1}, and $\Delta${ade17} (Table~\ref{yeastrls:genedeltable}). Growth curves later showed $\Delta${eos1} to be slow growing in our hands, so this strain was dropped from further work.

\begin{table}
	\includegraphics[width=1.0\linewidth]{yeastRLStable}
	\caption[Descriptions of the yeast single-gene deletion strains used in this work.]{Descriptions of the yeast single-gene deletion strains used in this work. Each gene deleted from yeast listed with the gene description per SGD \citep{cherry_saccharomyces_2011}, the RLS increase per McCormick 2015 \citep{mccormick_comprehensive_2015}, and whether the strain is documented as slow growing per growth rates reported in Giaever 2002 \citep{giaever_functional_2002}. **The $\Delta${eos1} strain, while not documented as slow growing, formed petite colonies in our hands, and was therefore excluded from the study.}
	\label{yeastrls:genedeltable}
\end{table}

\paragraph{Data independent acquisition mass spectrometry (DIA-MS)} Data were acquired using data-independent acquisition (DIA) on a Waters NanoAcquity UPLC coupled to a Thermo Q-Exactive HF Orbitrap mass spectrometer. Peptides were separated by reverse phase liquid chromatography using pulled tip columns created from 75 $\mu$m inner diameter fused silica capillary (New Objectives, Woburn, MA) in-house using a laser pulling device and packed with 3 $\mu$m ReproSil-Pur C18 beads (Dr. Maisch  GmbH, Ammerbuch, Germany) to 30 cm. Trap columns were created from 150 $\mu$um inner diameter fused silica capillary fritted with Kasil on one end and packed with the same C18 beads to 3 cm. Solvent A was 0.1\% formic acid in water (v/v), solvent B was 0.1\% formic acid in 98\% acetonitrile (v/v). For each injection, approximately 1 $\mu$g total protein was loaded and eluted using a 90-minute separating gradient starting at 5 and increasing to 35\% B, followed by a 40-minute wash and equilibration (total 130 minute method). DIA methods followed the chromatogram library workflow, described in greater detail elsewhere \citep{searle_chromatogram_2018}. Briefly, the control (reference) sample and calorie restricted yeast peptide samples were pooled to create a library sample, and a Thermo Q-Exactive HF was configured to acquire six gas phase fractions, each with 4 m/z DIA spectra using an overlapping window pattern from narrow mass ranges. For quantitative samples, the Thermo Q-Exactive HF was configured to acquire 25x 24 m/z DIA spectra using an overlapping window pattern from 388.43 to 1012.70 m/z. All DIA spectra were programmed with a normalized collision energy of 27 and an assumed charge state of +2.

Thermo RAW files were converted to .mzML format using the ProteoWizard package (version  3.0.10106), where they were centroided using vendor provided file reading libraries. Converted acquisition files were processed using EncyclopeDIA (version 0.7.0) configured with default settings (10 ppm precursor and fragment tolerances, considering only Y ions, and trypsin digestion was assumed). EncyclopeDIA features were submitted to Percolator (version 3.1) for validation at 1\% FDR.

\paragraph{Data analysis} Glucose concentration calculations and plots were performed using R. Peptide quantification was performed using EncyclopeDIA-derived peak picking, boundary settings, and transition refinement; peak integration values were calculated using Skyline \citep{maclean_skyline:_2010}. Consensus clustering was performed in R using the ConsensusClusterPlus package \citep{wilkerson_consensusclusterplus:_2010}. Dose-dependent abundance profiles were tested using the edgeR package \citep{storey_significance_2005}. Differential testing was performed in R using the MSstats package \citep{choi_msstats:_2014} (Appendix~\ref{appendix:msstats}).

\section{Determining key proteins that induce life span extension in yeast upon intervention with calorie restriction.}
Several metabolic intervention methods that extend RLS in yeast are also effective in multicellular organisms such as worms and mice \citep{wasko_yeast_2013}, suggesting broadly conserved mechanisms. Among these methods are calorie restriction and rapamycin treatment. Calorie restriction in yeast, induced by limiting glucose, extends RLS in genotype- and dose-dependent manners \citep{schleit_molecular_2013}; rapamycin, an inhibitor of the mTOR signaling pathway, influences nutrient sensing \citep{powers_extension_2006} and can be thought of as a mimetic for calorie restriction.

\begin{figure}
	\begin{center}
		\includegraphics[width=3in]{schema.png}
	\end{center}
	\caption[Yeast proteome profiles under six glucose concentrations spanning across calorie restriction.]{Yeast strain BY4741 was cultured under two glucose-abundant conditions (2\% and 1\% glucose in YEP), two glucose-restricted conditions (0.5\% and 0.05\% glucose in YEP), and two glucose-depleted conditions (0.005\% glucose in YEP and glycerol in YEP).}
	\label{calrest:schema}
\end{figure}

%\subsection{Peptide abundance shows no dose-dependent trend across glucose availability, but does capture proteins differential between high- and low-glucose availability}
Our motivation was to find a common protein signature in yeast cultured under CR conditions that corresponds to the calorie restriction mechanism and therefore increases lifespan (Figure~\ref{calrest:schema}). Calorie restriction is documented in yeast to occur when the glucose concentration is lowered from 2\% to between 0.05\% and 0.5\% \citep{kaeberlein_sir2-independent_2004}. Further, under caloric restriction, yeast replicative lifespan is extended 30-40\% \citep{lin_calorie_2002}. Therefore, our experimental design aimed to assess CR across a range of severities, spanning from abundant glucose through calorie restriction through fully depleted glucose, and including a glycerol control. 

%\begin{figure}[h!]
%	\begin{center}
%		\includegraphics[width=0.75\textwidth]{heatmap.png}
%	\end{center}
%	\caption[Proteome profiles visualize peptide response to varying severity of calorie restriction.]{Hierarchical clustering of both the 12,891 detected peptides and of the 17 individual samples.}
%	\label{calrest:heatmap}
%\end{figure}

%Hierarchically clustering peptides visually suggests that the data is composed of two groups (Figure~\ref{calrest:heatmap}). Below, we test the two-group hypothesis using consensus clustering to explore different values of \emph{k} and compare cluster membership under different sample labels.

\begin{figure}
	\begin{center}
		\includegraphics[width=0.75\textwidth]{dosedependency_500randompeps.png}
	\end{center}
	\caption[Peptide abundance shows no dose-dependent trends over glucose availability.]{Peptide abundance shows no dose-dependent trends over glucose availability. Time course analysis \citep{storey_significance_2005} was applied to test for significant trends in peptide abundance over the continuous glucose availability.}
	\label{calrest:dose}
\end{figure}

We might expect the proteome to respond in several ways. First, if the proteome responded in a dose-dependent manner, we would expect to see the proteins involved in CR to be differentially abundant at the different glucose concentrations. This may appear as a monotonic response (that is, proteins changing in abundance linearly with the amount of glucose available), or this may appear as a multimodal response (for example, with proteins differentially abundant only during CR conditions). To assess whether any proteins were differentially abundant in a dose-dependent manner, we used the EDGE software package to test if any proteins changed significantly compared to the mean abundance profile. However, no statistically significant trends were found in this data (Figure~\ref{calrest:dose}).

\begin{figure}
	\begin{center}
		\includegraphics[width=5in]{clustering.png}
	\end{center}
	\caption[Using initial or harvest glucose concentration labels cluster samples identically.]{Using initial or harvest glucose concentration labels cluster samples identically. Confusion matrices of k-means clustering with two values of \textit{k} and two sample labels.}
	\label{calrest:cluster}
\end{figure}

If not a dose-dependent response, we might expect that the yeast proteome would respond to calorie restriction via a \emph{switch} mechanism. In this scenario, proteins would only be differential in the CR condition, and not in either the glucose abundant or glucose depleted conditions. We tested this hypothesis using \emph{consensus clustering} \citep{wilkerson_consensusclusterplus:_2010}. In consensus clustering, a particular clustering technique is chosen (here, we used k-means clustering) and the data is clustered multiple times under different values of \textit{k}, then the clusters are themselves clustered. If a sample is reproducibly clustered in the same group, it is more likely that the sample belongs in that group. When consensus clustering was performed on the peptide abundance profiles, clustering by k-means confirmed a k=2 (that is, two groups of samples among the three conditions and six doses). Consensus clustering even robustly preserves these two groups when k is increased to k=3 only moving one replicate of glycerol to an outgroup (Figure~\ref{calrest:cluster}). 

Unfortunately, the k=2 consensus cluster also holds when the sample labels are changed to the final glucose concentrations as measured at harvest, with one group represented by all samples with zero glucose at harvest and the second group including samples with any glucose remaining at harvest. Because these two clusters are perfectly preserved when class labels are changed to glucose concentration at the time of harvest, these clustering results are inconclusive.

\begin{figure}
	\begin{center}
		\includegraphics[width=1.0\textwidth]{culture_glucose_continuous.png}
	\end{center}
	\caption[Cultures with low starting glucose concentrations rapidly consume their available glucose.]{Cultures with low starting glucose concentrations rapidly consume their available glucose. (a) The concentration of glucose available in the growth media was tested periodically during culture growth for all conditions and all replicates. (b) Yeast grown in 2\% glucose and 1\% glucose did not consume all the available glucose in their media, having roughly the 100\% of their initial glucose concentration available at the time of harvest. Cultures with lower initial glucose concentrations, especially the 0.05\% and 0.005\% glucose conditions,were depleted of all measurable glucose before the time of harvest.}
	\label{calrest:glucose}
\end{figure}

During culture growth, we periodically sampled each replicate of each culture to measure the OD600 and the glucose concentration in the media, making measurements roughly once per doubling after an initial overnight growth. We harvested the cultures at the same OD600 in the interest of controlling biomass across the samples. Because the yeast grow more rapidly under glucose abundant conditions, this meant that the control 2\% glucose condition was harvested earlier (chronologically) than the glucose deficient cultures. Unfortunately, this also meant the the glucose deficient conditions consumed or otherwise depleted nearly all of the available glucose in their media over their duration of incubation (Figure~\ref{calrest:glucose}a). From the glucose concentration measurements, we see that the three conditions wit hthe most abundant starting glucose concentrations (2\%, 1\%, 0.5\%) did not appreciably deplete their glucose, while the three most glucose deficient conditions (0.05\%, 0.005\%, glycerol) had consumed or otherwise depleted all available glucose before the cultures were harvested (Figure~\ref{calrest:glucose}b).

To our knowledge, no other study of CR in yeast has also performed an enzymatic assay to measure the concentration of free glucose available to the yeast during what is believed to be calorie restriction. This may have important consequences on what we believe to be the mechanism of calorie restriction and appropriate calorie restriction dosage.

\begin{figure}
	\begin{center}
		\includegraphics[width=1.0\textwidth]{hsp12.png}
	\end{center}
	\caption[Increased expression of proteins required for lifespan extension clusters in the deficient glucose group.]{Increased expression of proteins required for lifespan extension clusters in the deficient glucose group.The plasma membrane protein Hsp12 (YFL014W) is known to increase in response to DNA replication stress (a hallmark of molecular aging) and in response to calorie restriction. Coverage of this protein is high and abundance is increased 7.5-fold in the group comprised of 0.05\%, 0.005\%, and glycerol samples compared to the group comprised of the 2\%, 1\%, and 0.5\% glucose samples.}
	\label{calrest:hsp}
\end{figure}

%\subsection{Proteins required for lifespan extension by calorie restriction are more abundant in the CR group}
Despite the confounded labels, we analyzed the differential proteins between the two clusters. Using a t-test between the two clusters, we found a number of differentially abundant proteins, many of which are expected to be differential under glucose availability and even calorie restriction specifically. One of the most highly differential proteins after significance testing is Heat Shock Protein 12 (Hsp12) (Figure~\ref{calrest:hsp}a). Hsp12 is documented in the Saccharomycese Genome Database as being glucose-repressed (\url{https://www.yeastgenome.org/locus/S000001880}), lending credibility to the observed differential abundance. Not only is the overall protein abundance highly differential between the low and high glucose groups, but all the peptides detected for Hsp12 trend consistently (Figure~\ref{calrest:hsp}b) and the peptide chromatograms display ideal characteristics (Figure~\ref{calrest:hsp}c). Hsp12 is also documented in SGD as being required for calorie-restriction mediated lifespan extension, suggesting that the proteomic data here does capture the phenotype of lifespan extension.


\section{Constructing quantitative molecular phenotypes of yeast longevity for life-span modulating genotypes.}
As expected of a complex polygenic trait, several independent molecular pathways likely contribute to the ultimate aging phenotype. Existing work has identified multiple independent pathways that perturb cellular aging, including mitochondrial respiration. In this aim, I constructed molecular phenotypes for yeast mutants whose genotypes are associated with extended replicative lifespan. I hypothesized that molecular phenotypes would cluster into groups indicative of the complex functional processes underlying yeast longevity, yielding quantitative signatures of cellular aging. I performed differential protein abundance analysis with an ANOVA-based classic statistics approach, and finally clustered the proteome signatures into molecular phenotypes that represent mechanisms of yeast longevity. I hypothesized that the resulting molecular phenotypes would replicate known processes such as mitochondrial respiration, reveal novel protein members of established processes, and discover processes not yet associated with yeast longevity.

%\subsection{Genotypes associated with increased RLS share differential regulation of key proteins.}
The $\Delta${sgf73}, $\Delta${ubp8}, $\Delta${idh2}, and $\Delta${tor1} strains used in this work were selected to represent a range of replicative lifespan extension based on the results from McCormick 2015 \citep{mccormick_comprehensive_2015}. We obtained the strains used in their work, cultured them under our conditions, and performed an RLS assay to confirm the lifespan for the exact strains used in this work's proteomic analyses.

\begin{figure}
	\begin{center}
		\includegraphics[width=0.6\textwidth]{yeastRLSassay_results.png}
	\end{center}
	\caption[Survival curves for the yeast deletion strains used in this work.]{Survival curves for the yeast deletion strains used in this work. The results are complicated by the wildtype living very long (median lifespan was 37.5, normal wildtype should be closer to 27). Even with the very long-lived wildtype, $\Delta${sgf73} and $\Delta${udp8} appeared long. (n=40 cells started per genotype)}
	\label{yeastRLS:RLSresults}
\end{figure}

Although strains $\Delta${sgf73} and $\Delta${ubp8} were confirmed long-lived versus the control BY4741, the control strain itself lived longer than expected based on previous RLS assays (Figure~\ref{yeastRLS:RLSresults}). Because of this, the other two strains selected to represent RLS extension ($\Delta${idh2} and $\Delta${tor1}) appear not to be long lived, at least these particular strains. 

\begin{figure}
	\begin{center}
		\includegraphics[clip, trim=0mm 0mm 0mm 10mm, width=1.0\textwidth]{msstats_c_VolcanoPlot.pdf}
	\end{center}
	\caption[Volcano plot of protein abundances in the RLS-extending genotypes versus the control genotypes.]{Volcano plot of protein abundances in the RLS-extending genotypes versus the control genotypes. }
	\label{yeastRLS:volcano_rls}
\end{figure}

We performed differential protein abundance testing on the selected long-lived strains ($\Delta${sgf73}, $\Delta${ubp8}, $\Delta${idh2}, and $\Delta${tor1}) versus the control strains ($\Delta${ade17}, $\Delta${adp1}, BY4741 replicates cultured at the same time as the deletion strain replicates, and two BY4741 external reference cell pellets cultured at a different time but prepared alongside these strains). If a common signature of protein abundance was associated broadly across all RLS-extending genotypes, we might expect those proteins would be significant in a simple pairwise comparison. In total, we found 74 of 3553 detected proteins were significantly differential between the long-lived strains and the control strains (Figure~\ref{yeastRLS:volcano_rls}, Appendix~ \ref{appendix:rls_fc_table1}, Appendix~ \ref{appendix:rls_fc_table2}). PANTHER GO-Slim Biological Process analysis comparing these 74 proteins against the background of all yeast genes found that tricarboxylic acid cycle and carbohydrate metabolic processes are overrepresented. PANTHER GO-Slim Molecular Function analysis additional finds that catalytic activity is overrepresented by these proteins. 

\begin{figure}
	\begin{center}
		\includegraphics[page=1, clip, trim=0mm 0mm 0mm 10mm, width=1.0\textwidth]{msstats_VolcanoPlot.pdf}
	\end{center}
	\caption[Volcano plot of protein abundances in $\Delta${ade17} vs BY4741.]{Volcano plot of protein abundances in $\Delta${ade17} vs BY4741. }
	\label{yeastRLS:volcano_ade17}
\end{figure}

\begin{figure}
	\begin{center}
		\includegraphics[page=2, clip, trim=0mm 0mm 0mm 10mm, width=1.0\textwidth]{msstats_VolcanoPlot.pdf}
	\end{center}
	\caption[Volcano plot of protein abundances in $\Delta${adp1} vs BY4741.]{Volcano plot of protein abundances in $\Delta${adp1} vs BY4741. }
	\label{yeastRLS:volcano_adp1}
\end{figure}

To compare these high-level results between RLS-extension vs all other strains, we next performed pairwise comparisons between all single-gene deletions and the control BY4741 strain. The two negative control deletion strains, $\Delta${ade17} (Figure~\ref{yeastRLS:volcano_ade17}) and $\Delta${adp1} (Figure~\ref{yeastRLS:volcano_adp1}) (Appendix~ \ref{appendix:rls_fc_table1}), show few differentially abundant proteins compared to the BY4741 experimental control. We might expect that these two strains would not have many differential proteins based on the number of known genetic interactors listed in SGD (Table~\ref{yeastrls:genedeltable}).

\begin{figure}
	\begin{center}
		\includegraphics[page=4,clip, trim=0mm 0mm 0mm 10mm, width=1.0\textwidth]{msstats_VolcanoPlot.pdf}
	\end{center}
	\caption[Volcano plot of protein abundances in $\Delta${idh2} vs BY4741.]{Volcano plot of protein abundances in $\Delta${idh2} vs BY4741. }
	\label{yeastRLS:volcano_idh2}
\end{figure}

\begin{figure}
	\begin{center}
		\includegraphics[page=6, clip, trim=0mm 0mm 0mm 10mm, width=1.0\textwidth]{msstats_VolcanoPlot.pdf}
	\end{center}
	\caption[Volcano plot of protein abundances in $\Delta${tor1} vs BY4741.]{Volcano plot of protein abundances in $\Delta${tor1} vs BY4741. }
	\label{yeastRLS:volcano_tor1}
\end{figure}

The two strains chosen to represent a low RLS-extension but did not appear to be long-lived based on our in-house RLS assay, $\Delta${idh2} (Figure~\ref{yeastRLS:volcano_idh2}) and $\Delta${tor1} (Figure~\ref{yeastRLS:volcano_tor1}), both show slight more differentially abundant proteins compared to the BY4741 experimental control. Again, based on the number of known interactions for these two genes (Table~\ref{yeastrls:genedeltable}), these results align with what we expect.

\begin{figure}
	\begin{center}
		\includegraphics[page=5, clip, trim=0mm 0mm 0mm 10mm, width=1.0\textwidth]{msstats_VolcanoPlot.pdf}
	\end{center}
	\caption[Volcano plot of protein abundances in $\Delta${sgf73} vs BY4741.]{Volcano plot of protein abundances in $\Delta${sgf73} vs BY4741. }
	\label{yeastRLS:volcano_sgf73}
\end{figure}

\begin{figure}
	\begin{center}
		\includegraphics[page=7, clip, trim=0mm 0mm 0mm 10mm, width=1.0\textwidth]{msstats_VolcanoPlot.pdf}
	\end{center}
	\caption[Volcano plot of protein abundances in $\Delta${ubp8} vs BY4741.]{Volcano plot of protein abundances in $\Delta${ubp8} vs BY4741. }
	\label{yeastRLS:volcano_ubp8}
\end{figure}

Last, the two strains both documented as extending RLS and proven in our hands to display RLS extension, $\Delta${sgf73} (Figure~\ref{yeastRLS:volcano_sgf73}) and $\Delta${ubp8} (Figure~\ref{yeastRLS:volcano_ubp8}), show many differentially abundant proteins compared to the BY4741 experimental control. This also matched our expectations based on the number of unique interactions for these two genes (Table~\ref{yeastrls:genedeltable}).

Of the 3553 proteins tested for differential abundance, 74 were determined significant when comparing the RLS genotypes versus the control genotypes. If we compare the results between the two group test with the results from the individual genotype tests, only 38 of those proteins were significant in at least one of the single genotype vs BY4741 tests (Appendix~\ref{appendix:rls_fc_table1}). Further, of those 38 proteins, six were similarly differential in all genotypes and an additional two were differential in all but one genotype, suggesting that they may be involved with general response to the gene deletion process but are not likely involved in the extended RLS phenotype. Of the remaining 30 candidate proteins, three proteins are significant in three genotypes, five are significant in two genotypes, and the remaining are only significant in one genotype. This casual comparison suggests that there may not be a global signature of yeast RLS extension, at least using protein abundance data.

Because no clear protein signature is evident from comparing all four RLS-extending genotypes to the controls, we next split the genotypes into a high RLS extension group ($\Delta${sgf73} and $\Delta${ubp8}), a low RLS extension group ($\Delta${idh2} and $\Delta${tor1}), a non-RLS extending group ($\Delta${ade17} and $\Delta${adp1}), and compared each of these three groups to the control BY4741. We additionally tested the external reference BY4741 cell pellet against the experimental control BY4741 as a sanity check, which found 14 significantly differential proteins disregarded from further consideration. We also found that three of the significantly differential proteins in this multigroup comparison were Idh2, Sgf73, and Ade17, which validates that the methods is working as intended, since these three proteins are three of the knocked out genes and therefore should indeed be absent from those strains. 

When we compare the other differential proteins, we find that the low RLS group had 46 differential proteins; the high RLS group had 66. The groups shared 17 proteins differentially regulated at similar fold change which are affiliated with \emph{ATP synthesis coupled electron/proton transport} per PANTHER overrepresentation testing, although this is a low number of proteins to use for overrepresentation testing. It's not surprising that $\Delta${idh2} may show differential proteins with this type of biological process annotation, as \textit{idh2} is a subunit of a complex involved with the TCA cycle, but it is surprising to see that the extended RLS strains, $\Delta${sgf73} and $\Delta${ubp8}, also show an increase in those ATP synthesis-related proteins (Appendix~\ref{appendix:rls_fc_table3}), since those two genes are part of the SAGA complex, which is typically associated with chromatin remodeling and not ATP synthesis. The differential proteins unique to the high-RLS genotypes did not have any significant overrepresentation; however, the differential proteins unique to the low-RLS genotypes was also enriched for ATP synthesis coupled electron transport.

We can further determine which of these results is reasonable by looking back at the original mass spectrometry data. In particular, we can consider which of these significant proteins is represented by multiple peptides, which of those peptides is represented by multiple high-quality fragment ion chromatograms, and whether those peptides trend in the same abundance pattern across the RLS-extending genotypes. If these three conditions are met, the protein candidate should be considered more seriously. When we curate the 75 proteins differential in the RLS-extending groups, but not differential in the control and non-RLS extending groups, we quickly refine to 35 proteins with at least two unique peptides with high-quality fragment ion chromatograms. Refined proteins are nearly all lower abundance in the RLS extended groups, and are associated with cellular respiration, carboxylic acid metabolic processes, and nucleobase-containing small molecule metabolic processes.


\section {Conclusions}

The mechanisms of aging and life span modulation likely include multiple cellular processes; however, the range of molecular phenotypes that cells display as they age under different contexts and genetic backgrounds is not well understood. This work used state-of-the-art biochemical and proteomic methods to measure the molecular phenotypes of life span extension in yeast to reveal novel protein members of established processes and discover processes not yet associated with yeast longevity. Understandably these pathologies are complex and likely involve mechanisms beyond the differential expression of a specific protein or even a set of the same proteins. Our work is validated through several control measures that confirm the methods and data are appropriate, and we recapitulate known basic biology of aging, such as HSP12 association with calorie restriction. In addition, our differential proteomics reveals novel insights that generate hypotheses for further functional or spaciotemporal investigations. In particular, this chapter highlights two possible directions for future work: finer-tuned calorie restriction conditions to distinguish between the switch mechanism this work suggests and a potential calorie restriction level between 0.5\% and 0.05\% glucose concentration; and testing additional RLS-modulating genotypes to further stratify categories of RLS extension. With increasing quantitative proteomics projects being undertaken with at-scale methods such as the DIA-MS approaches used in this chapter, combining experiments together into larger data sets will likely afford more statistical power and increased ability to construct more sensitive and specific molecular phenotypes of complex pathologies.
